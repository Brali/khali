<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
        <link rel="stylesheet" href="authentification.css" />
    </head>
    <body>
        <?php session_start();

        if (!isset($_POST['email'], $_POST['password']))
            exit('need data');
        include('../identifiants.php');
        $dsn='mysql:dbname='.$nomBDD.';host='.$urlBDD;
        $user=$loginBDD;
        $password=$mdpBDD;
        
        try{
            $dbh=new PDO($dsn,$user,$password); 
        }catch(PDOException $e){
            echo'Connexion échouée:'.$e->getMessage();
            exit();
        }
             
        $sql = "SELECT count(*) as total FROM user WHERE email=:email";
        $resultats = $dbh->prepare($sql);
        $resultats->bindParam(":email", $_POST['email']);
        $resultats->execute(); 
        $row= $resultats->fetch(PDO::FETCH_ASSOC);
        if($row ["total"] != 0){
             echo("E-mail déjà existant");
             exit();
        }
        
        $sql = "INSERT INTO user (email, password) VALUES (:email,:password)";
        $resultats = $dbh->prepare($sql);
        $resultats->bindParam(":email", $_POST['email']);
        $resultats->bindParam(":password", $_POST['password']);
        $resultats->execute();

        $_SESSION['email']=$_POST['email'];
        echo "Incrit ".$_SESSION['email'];
        ?>
        <script>
            let user = new User("<?=$_SESSION['email']?>");
            console.log(user);
            sessionStorage.setItem("login", user.email);
        </script>
    </body>
</html>