<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
        <link rel="stylesheet" href="ticket.css" />
    </head>
    <body>
        <form action="" method="GET">
            <div class="form-group">
                <label for="ticket-id-chooser">Choisir un ticket à visualiser</label>
                <select name="id" onchange="form.submit()" class="form-control" id="ticket-id-chooser">
                <?php
                include('../identifiants.php');
                $dbh = new PDO('mysql:dbname='.$nomBDD.';host='.$urlBDD, $loginBDD, $mdpBDD);
                $request = $dbh->prepare("SELECT id,sujet FROM ticket");
                $request->execute(); 
                foreach ($request->fetchAll(PDO::FETCH_OBJ) as $ticket) {
                    ?>
                    <option value="<?=$ticket->id?>"<?=isset($_GET['id'])&&$_GET['id']==$ticket->id?'selected':''?>>Ticket n°<?=$ticket->id?> : <?=$ticket->sujet?></option>
                    <?php
                }
                ?>
                </select>
            </div>
        </form>
        <br />
        <?php
        if (isset($_GET['id'])) {
            $request = $dbh->prepare("SELECT * FROM ticket WHERE id = :id");
            $request->bindParam(":id", $_GET['id']);
            $request->execute(); 
            $ticket = $request->fetch(PDO::FETCH_OBJ);
            ?>
            <h3>Ticket n°<?=$_GET['id']?> : <?=$ticket->sujet?></h3>
            <p><?=$ticket->description?></p>
            <p><b>Priorité :</b> <?=$ticket->prio?></p>
            <p><b>Secteur :</b> <?=$ticket->secteur?></p>
            <p><b>Auteur : </b><?=$ticket->login?></p>
            <a href="modifierTicket.php?id=<?=$ticket->id?>"><button class="btn btn-primary">Modifier</button></a>
            <a href="afficheListeTickets.php"><button class="btn btn-primary">Liste des tickets</button></a>
            <?php
        } else {
        ?>
            Aucun identifiant de ticket spécifié
        <?php
        }
        ?>
    </body>
</html>