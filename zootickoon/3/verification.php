<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
        <link rel="stylesheet" href="authentification.css" />
    </head>
    <body>
        <?php
        $utilisateurs =["Lina@gmail.com","passeLina","Edgar@gmail.com","passeEdgar"];

        if (!isset($_POST['email'], $_POST['password']))
            exit('need data');

            //La méthode POST met les données dans le corps de la requête alors que la méthode GET, il n'y a pas  de corps doù le fait que cela affiche toutes les données dans l'URL.
        for ($i = 0; $i < count($utilisateurs); $i+=2) {
            if ($_POST['email']==$utilisateurs[$i] && $_POST['password']==$utilisateurs[$i+1]) {
                session_start();
                $_SESSION['email'] = $utilisateurs[$i];
                $logged = true;
                break;
            }
        }
        $date = date("d/m/y  H:i "); 
        file_put_contents('long.log', $_POST['email'].' '.$_POST['password'].' '.$date."\n", FILE_APPEND);
        if ($logged) {
        ?>
        Connecté <?=$_SESSION['email']?>
        <?php
        $connexionsJSON = file_get_contents('login.json');
        $connexions=json_decode($connexionsJSON);
        $log = new class {};
        $log->email = $_POST['email'];
        $log->mdp = $_POST['password'];
        $log->date = $date;
        array_push($connexions->logs,$log);
        $connexionsJSON=json_encode($connexions,JSON_PRETTY_PRINT);
        file_put_contents('login.json', $connexionsJSON);
        } else {
        ?>
         <span class="border border-danger"> Invalid connection</span>
        <?php
        }
        ?>
       
        
    </body>
</html>