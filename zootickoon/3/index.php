<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="index.css" />
        <title>Ma page de test</title>
    </head>
    <body>
        <h1>
            Chapreisti
        </h1>
        <ul class="menu">
            <li><a href=".">Description of the zoo</a></li>
            <li><a href="#">Animation</a></li>
            <li><a href="#">Shop</a></li>
            <li><a href="ticket.html">Ticket</a></li>
            <li><a href="#">Wordpress</a></li>
        </ul>
        <div class="secteurs">
            <div class="secteur">
                <h2>
                    Europe 
                </h2>
                <img src="../assets/lynx europe.jpg">
                <span class="code">
                    #Jmas
                </span>
                <p>
                   The different felines in Europe
                </p>
                <span class="surface">
                    Surface in m²
                </span>
                <span class="nombreA">  
                    404/500
                </span>
            </div>
            <div class="secteur">
                <h2>
                    Asia
                </h2>
                <img src="../assets/chat léopard asie.jpg">
                <span class="code">
                    #Thithuthuy
                </span>
                <p>
                    The different felines in Asia
                </p>
                <span class="surface">
                    Surface in m²
                </span>
                <span class="nombreA">
                    404/500
                </span>
            </div>
        </div>
        <div class="secteurs">
            <div class="secteur">
                <h2>
                    America
                </h2>
                <img src="../assets/Ocelot amerique.jpg">
                <span class="code">
                    #masnada
                </span>
                <p>
                    The different felines in America
                </p>
                <span class="surface">
                    Surface in m²
                </span>
                <span class="nombreA">
                    404/500
                </span>
            </div>
            <div class="secteur">
                <h2>
                    Africa
                </h2>
                <img src="../assets/serval afrique.jpg">
                <span class="code">
                    #PPL
                </span>
                <p>
                    The different felines in Africa
                </p>
                <span class="surface">
                    Surface in m²
                </span>
                <span class="nombreA">
                    404/500
                </span>
            </div>
        </div>
        <?php $heure = date("H"); 
            echo $heure; 
            if($heure>8 && $heure<13){
                echo '<img src="../assets/zebre.jpg">';
            }
            else if($heure>=13 && $heure<20){
                echo '<img src="../assets/girafe.jpg">';
            }
            else{
                echo '<img src="../assets/panda.jpg">';
            }   
            ?>
    </body>
    
</html>